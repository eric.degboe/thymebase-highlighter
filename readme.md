## Libraries used
- sinatra: Used for quickly creating web applications [read more here](http://sinatrarb.com/)

## Getting Started
1- Clone or download this project from the repository.

2- After cloning you need to install project dependencies depending on your packages manager by running `bundle install`.

## Development server
Run `ruby main.rb`. Navigate to `http://localhost:4567`. And enjoy your highlighter.

## Demo
You will find a Demo or the working project [here](https://thymebase-highlighter.herokuapp.com/)

## Other details
Don't hesitate to ask me 😉.
