class Highlighter
  def initialize(content, highlights)
    @content = content
    @highlights = highlights
    @colors = []
  end

  # Highlight and convert paragraph
  def convert
    convert_to_html_paragraph(highlight())
  end

  # Convert to paragraph
  def convert_to_html_paragraph(text)
    parts = text.split(/\n+/)
    result = ""
    parts.each do |part|
      result = result + "<p>#{part}</p>"
    end
    result
  end

  # Highlight the content
  def highlight
    # Get each highlight value and generate colors for them
    @highlights.each_with_index do |highlight, index|
      @highlights[index][:id] = index
      @highlights[index][:value] = @content[highlight[:start]...highlight[:end]+1]
      @highlights[index][:color] = generate_random_color()
      @highlights[index][:converted] = ""
    end

    # Get each highlight parent if it has one
    @highlights.each_with_index do |highlight, index|
      @highlights[index][:contained_in] = contained_in(highlight)
    end

    # Convert each highlight in HTML tag
    @highlights.each_with_index do |highlight, index|
      # Only do it for primary highlight because they will include their childs
      next unless highlight[:contained_in].nil?
      @highlights[index][:converted] = convert_highlight_to_html(highlight[:id])
    end

    # Generate the complete highlighted content
    converted_highlights = @highlights.select{ |highlight| highlight[:contained_in].nil? }
    converted_highlights = converted_highlights.sort_by { |h| h[:start] }

    highlight_content = @content[0...converted_highlights[0][:start]]

    converted_highlights.each_with_index do |highlight, index|
      highlight_content = highlight_content + highlight[:converted]
      if converted_highlights[index+1].nil?
        highlight_content = highlight_content + @content[highlight[:end]+1...@content.length]
      else
        highlight_content = highlight_content + @content[highlight[:end]+1...converted_highlights[index+1][:start]]
      end
    end

    highlight_content
  end

  private

  def generate_random_color
    color = ''
    loop do
      r = rand(0..255)
      g = rand(0..255)
      b = rand(0..255)
      color = "#{r}, #{g}, #{b}"
      break unless @colors.include? color
    end
    @colors << color
    color
  end

  # Return the highlight element in wich element is contained
  def contained_in(element)
    container_id = nil
    @highlights.each do |highlight|
      if highlight[:start] <= element[:start] && highlight[:end] >= element[:end] && highlight[:id] != element[:id]
        if container_id.nil?
          container_id = highlight[:id]
        else
          if highlight[:start] >= @highlights[contain][:start] && highlight[:end] <= @highlights[contain][:end]
            container_id = highlight[:id]
          end
        end
      end
    end
    container_id
  end

  # Convert a highlight with ID {id} into HTML code
  def convert_highlight_to_html(id)
    highlight_childs = @highlights.select{ |highlight| highlight[:contained_in] == id }

    # if the highlight do not have any child we can  convert it directly
    if highlight_childs.length == 0
      "<strong class='tooltip-container' style='color: rgb(#{@highlights[id][:color]});'>#{@highlights[id][:value]}<span class='tooltip' style='border-color: rgb(#{@highlights[id][:color]}); background-color: rgb(#{@highlights[id][:color]});'>#{@highlights[id][:comment]}</span></strong>"
    else
      # The highlight has children so we need to convert each of them first
      value = @highlights[id][:value]
      highlight_childs = highlight_childs.sort_by { |c| c[:start] }
      converted_html = value[0...highlight_childs[0][:start]-@highlights[id][:start]]

      highlight_childs.each_with_index do |child, index|
        converted_html = converted_html + convert_highlight_to_html(child[:id])
        if highlight_childs[index+1].nil?
          converted_html = converted_html + value[child[:end]-@highlights[id][:start]+1...value.length]
        else
          converted_html = converted_html + value[child[:end]-@highlights[id][:start]+1...highlight_childs[index+1][:start]-@highlights[id][:start]]
        end
      end

      "<strong class='tooltip-container' style='color: rgb(#{@highlights[id][:color]});'>#{converted_html}<span class='tooltip' style='border-color: rgb(#{@highlights[id][:color]}); background-color: rgb(#{@highlights[id][:color]});'>#{@highlights[id][:comment]}</span></strong>"
    end
  end
end
